# Frameworks/Libs used

Play, Retrofit(for retrieving trends data from rest api), Rome(for parsing rss feed)

I'm java developer(Spring), and working with Scala + Play was kinda new to me :)

I've tried to do everything there as much FP as possible, so stuff is pure and immutable

# Usage

Just access http://url/news/feedname

For example:

http://localhost:9000/news/asia

# Comments

There's some comments in code here and there. If you care you may read them :)

# Api key

I do provide api key here(creds.conf) for demo. It's free account, so nothing special.

# Architecture

The app is divided into three big parts with facade class wriring them all together:

* Retrieving of RSS data
* Retrieving of trends from rest api(note: because google does not provide api to it and unofficial api(http://www.google.com/trends/fetchComponent) to it was closed years ago and there's really no adequate way to fetch data from trends, i used wordtracker.com here which is really similar service to g trends but with api for devs)
* Simple algorithm for filtering data by trends based on regexps 

I've tried to keep "single responsibility principle" here as much as possible so you'll encounter lots of really small classes that do only one thing

# Possible improvemements

* use futures/akka for async rss/trend data loading
* use java timers api to do everything "in background" time from time and only provided already filtered data when user is accessing api instead of fetching everything each time 
* provide the cache to store some preloaded data from rss and trends api
* use lombok to generate getters and setters
* rewrite retrofit releated code to scala. i've written it in java because of some weird issues i had.
* improve the word searching algorithm itself
