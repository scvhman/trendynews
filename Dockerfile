### STAGE 1: Build ###
# there is no actual official sbt image lol
FROM bigtruedata/sbt as builder

ADD ./app /build-target/app
ADD ./build.sbt /build-target/build.sbt
ADD ./project/build.properties /build-target/project/build.properties
ADD ./project/plugins.sbt /build-target/project/plugins.sbt
RUN mkdir -p /build-targt-project/project
WORKDIR /build-target

RUN sbt dist
WORKDIR /build-target/target/universal
RUN mkdir -p /app
RUN unzip -d /app *.zip
RUN rm /app/bin/*.bat
RUN mv /app/bin/* /app/bin/start

### STAGE 2: Run ###

FROM openjdk:8-jre as runner
EXPOSE 9000

COPY --from=builder /app /app
CMD /app/bin/start