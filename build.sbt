name := "test"

version := "1.0"

lazy val `test` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(jdbc,
  ehcache,
  ws,
  specs2 % Test,
  guice,
  "com.rometools" % "rome" % "1.10.0",
  "com.rometools" % "rome-fetcher" % "1.10.0",
  "com.rometools" % "rome-modules" % "1.10.0",
  "com.rometools" % "rome-utils" % "1.10.0",
  "com.squareup.retrofit2" % "retrofit" % "2.4.0",
  "com.squareup.retrofit2" % "converter-jackson" % "2.4.0"
)

      