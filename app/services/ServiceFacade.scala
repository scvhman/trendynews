package services

import javax.inject.Inject
import services.compose.TrendFilter
import services.rss.{RssNewsEntity, RssReader}
import services.trends.impl.TrendDataWorker

class ServiceFacade @Inject()(rssReader: RssReader, trendFilter: TrendFilter, trendDataWorker: TrendDataWorker) {

  def getNews(feedName: String): List[RssNewsEntity] = {
    val feeds = rssReader.getAllNews(feedName)
    val trends = trendDataWorker.getAllTrends(feedName)
    trendFilter.retrieveOnlyTrended(trends, feeds)
  }
}
