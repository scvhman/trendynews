package services.trends.impl

import javax.inject.Inject
import services.trends.impl.config.{AuthProvider, ParamsProvider}
import services.trends.impl.data.entities.TrendDataReceived

import scala.collection.JavaConverters._

class TrendDataWorker @Inject()(retrofitSource: RetrofitSource,
                                authProvider: AuthProvider,
                                paramsProvider: ParamsProvider) {

  @throws(classOf[RuntimeException])
  def getAllTrends(query: String): List[TrendDataReceived] = {
    val client = retrofitSource.getClient
    val response = client
      .getListOfTrendsForKeyword(query, authProvider.getAppId(),
        authProvider.getAppKey(), paramsProvider.getMaxNumberOfTrends())
      .execute()
    //exception handling may be better with custom exception classes
    response.isSuccessful match {
      case true => response.body.getResults.asScala.toList
      case false => throw new RuntimeException("Error code " + response.code())
    }
  }
}
