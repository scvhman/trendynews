package services.trends.impl.config

import javax.inject.Inject
import play.api.Configuration

class ParamsProvider @Inject()(configuration: Configuration) {

  def getMaxNumberOfTrends(): Long = {
    //i ignore the option monad here because it will 100% exist here
    configuration.getLong("trends.amount").get
  }
}
