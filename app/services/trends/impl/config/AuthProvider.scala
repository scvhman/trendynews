package services.trends.impl.config

import javax.inject.Inject
import play.api.Configuration

//i ignore the option monads here because they will 100% exist here
class AuthProvider @Inject()(configuration: Configuration) {

  def getAppId(): String = {
    configuration.getString("creds.app.id").get
  }


  def getAppKey(): String = {
    configuration.getString("creds.app.key").get
  }
}
