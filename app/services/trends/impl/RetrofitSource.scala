package services.trends.impl

import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import services.trends.impl.data.TrendsRepository

class RetrofitSource {

  private val url = "http://api3.wordtracker.com"
  private var retrofitClient: TrendsRepository = _

  def getClient = {
    //kind of lazy loading.
    if (retrofitClient == null) {
      initRetrofit()
    }
    retrofitClient
  }

  private def initRetrofit() = {
    val retrofit = new Retrofit.Builder()
      .baseUrl(url)
      .client(getOkHttpClient())
      .addConverterFactory(JacksonConverterFactory.create(new ObjectMapper()))
      .build()
    retrofitClient = retrofit.create(classOf[TrendsRepository])
  }

  private def getOkHttpClient(): OkHttpClient = {
    new OkHttpClient().newBuilder().build()
  }
}
