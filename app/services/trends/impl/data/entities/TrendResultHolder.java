package services.trends.impl.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrendResultHolder {

	private List<TrendDataReceived> results;

	public List<TrendDataReceived> getResults() {
		return results;
	}

	public void setResults(List<TrendDataReceived> results) {
		this.results = results;
	}
}
