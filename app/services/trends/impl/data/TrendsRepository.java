package services.trends.impl.data;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import services.trends.impl.data.entities.TrendResultHolder;

public interface TrendsRepository {

	@GET("/search/stats")
	Call<TrendResultHolder> getListOfTrendsForKeyword(@Query("keywords[]") String query,
													  @Query("app_id") String appId,
													  @Query("app_key") String key,
													  @Query("limit") long limit);
}
