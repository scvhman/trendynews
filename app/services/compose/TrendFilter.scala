package services.compose

import services.rss.RssNewsEntity
import services.trends.impl.data.entities.TrendDataReceived

import scala.util.matching.Regex

class TrendFilter {

  def retrieveOnlyTrended(trends: List[TrendDataReceived], feeds: List[RssNewsEntity]): List[RssNewsEntity] = {
    //the point of doing such thing here is to get
    val trendsRegex = combineTrendsInOneRegex(trends.map(trend => trend.getKeyword))
    feeds.filter(news => {
      checkIfTextContainsTrend(news, trendsRegex)
    })
  }

  private def combineTrendsInOneRegex(trends: List[String]): Regex = {
    trends.mkString("|").r
  }

  private def checkIfTextContainsTrend(entity: RssNewsEntity, trends: Regex): Boolean = {
    trends.findAllIn(entity.title).hasNext || trends.findAllIn(entity.contents).hasNext
  }
}
