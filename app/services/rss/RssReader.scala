package services.rss

import javax.inject.Inject
import services.rss.util.{RssFeedProvider, RssUrlGenerator}

class RssReader @Inject()(rssUrlGenerator: RssUrlGenerator, rssFeedProvider: RssFeedProvider) {

  def getAllNews(feedName: String): List[RssNewsEntity] = {
    val rssUrl = rssUrlGenerator.makeUrl(feedName)
    rssFeedProvider.getFeed(rssUrl)
  }
}
