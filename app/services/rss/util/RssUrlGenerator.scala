package services.rss.util

class RssUrlGenerator {

  //hardcode here but it wont be hard to store everything in db and then fetch
  private val basicUrl = "http://rss.cnn.com/rss/edition_"
  private val basicRes = ".rss"

  def makeUrl(name: String): String = {
    basicUrl + name + basicRes
  }
}
