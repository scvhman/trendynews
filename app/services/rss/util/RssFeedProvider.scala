package services.rss.util

import java.net.URL

import com.rometools.rome.feed.synd.SyndFeed
import com.rometools.rome.io.{SyndFeedInput, XmlReader}
import services.rss.RssNewsEntity

import scala.collection.JavaConverters._

class RssFeedProvider {

  def getFeed(url: String): List[RssNewsEntity] = {
    //converting this stuff to scala list for convenience
    val posts = getRomeReader(url)
      .getEntries
      .asScala.toList
    posts.map(f => {
      new RssNewsEntity(f.getTitle, f.getLink, f.getDescription.getValue)
    })
  }

  private def getRomeReader(url: String): SyndFeed = {
    //standart rome api, actually you can throw there any xml parser if you like to do it manually because rss is just xml
    new SyndFeedInput().build(new XmlReader(new URL(url)))
  }
}
