package controllers

import javax.inject.Inject
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents}
import services.ServiceFacade

class ServiceController @Inject()(cc: ControllerComponents, serviceFacade: ServiceFacade) extends AbstractController(cc) {

  def getNews(feed: String) = Action {
    //i know that there is reads and writes + format
    Ok(Json
      .toJson(serviceFacade
        .getNews(feed)
        .map(data => Map(
          "title" -> data.title,
          "link" -> data.link,
          "contents" -> data.contents))))
  }
}
